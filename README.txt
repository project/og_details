NAME
--------------------------------
Organic Groups - Configurable Details Block


DESCRIPTION
--------------------------------

This module creates a more configurable alternative to the default OG Details block by allowing you to:

* Limit create links by integrating w/ OG Audience By Type
* Hide invite link
* Hide member count link
* Hide group manager link
* Make group manager into plain text
* Hide Join & My Membership links

INSTRUCTIONS
--------------------------------

* Enable the module at admin/build/modules/list
* Go the block settings page admin/build/block/list and click 'Configure' next to the 'OG Configurable Details Block'

CREDITS
--------------------------------
* Authored and maintained by Matthew Grasmick (madmatter23 on drupal.org)
* Some development sponsored by Blue Water Media (http://www.bluewatermedia.com